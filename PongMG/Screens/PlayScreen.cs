﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

using MonoGame.Utils;

namespace PongMG
{
	public class PlayScreen : GameState
	{
		int scorePlayer1, scorePlayer2, speed;
		SoundEffect bounce, goal;
		string scoreText;
		Vector2 center, scoreSize, player1Motion, player2Motion;
		Texture2D wall;
		Bat Player1, Player2;
		public Ball ball;

		public PlayScreen(Game game, GameStateManager gsm) : base(game, gsm)
		{
			center = new Vector2(Root.ScreenWidth / 2, Root.ScreenHeight / 2);
			speed = 6;
		}

		protected override void LoadContent()
		{
			base.LoadContent();

			wall = Content.Load<Texture2D>("Art/wall");

			Texture2D texture;
			texture = Content.Load<Texture2D>("Art/bat");
			Player1 = new Bat(texture, new Vector2(40, center.Y - texture.Height / 2));
			Player2 = new Bat(texture, new Vector2(1210, center.Y - texture.Height / 2));

			texture = Content.Load<Texture2D>("Art/ball");
			ball = new Ball(texture);

			bounce = Content.Load<SoundEffect>("Sound/bounce");
			goal = Content.Load<SoundEffect>("Sound/goal");
		}

		public override void Update(GameTime gameTime)
		{
			if (InputHandler.KeyPressed(Keys.Space))
				Debug.LogToScreen("Test output to generate a longer list", gameTime);

			if (ball.Speed <= 0) {
				if (InputHandler.KeyPressed(Keys.H))
					ball.Speed = 5f;
			}

			ball.Update(gameTime);
			PlayerControls(gameTime);
			CheckCollisions(gameTime);

			Debug.PutOnScreen("BallPos", "Ball position: " + ball.Position.X + ", " + ball.Position.Y);

			base.Update(gameTime);
		}

		public override void Draw(GameTime gameTime)
		{
			Root.Batch.Begin();

			base.Draw(gameTime);
			DrawField();

			Root.Batch.End();
		}

		void DrawField()
		{
			scoreText = scorePlayer1.ToString();
			scoreSize = Font36.MeasureString(scoreText);

			Root.Batch.DrawString(
				Font36,
				scoreText,
				new Vector2(center.X - 70 - scoreSize.X / 2, 50),
				Color.White);

			scoreText = scorePlayer2.ToString();
			scoreSize = Font36.MeasureString(scoreText);

			Root.Batch.DrawString(
				Font36,
				scoreText,
				new Vector2(center.X + 70 - scoreSize.X / 2, 50),
				Color.White);

			Root.Batch.Draw(wall,
							new Vector2(center.X - wall.Width / 2, 0),
							Color.White);

			Player1.Draw(Root.Batch);
			Player2.Draw(Root.Batch);
			ball.Draw(Root.Batch);
		}

		void PlayerControls(GameTime gameTime)
		{
			player1Motion = Vector2.Zero;
			player2Motion = Vector2.Zero;

			if (InputHandler.KeyDown(Keys.A))
				player1Motion.Y = 1;
			else if (InputHandler.KeyDown(Keys.Q))
				player1Motion.Y = -1;

			if (player1Motion != Vector2.Zero) {
				player1Motion.Normalize();
				Player1.Move(player1Motion * speed);
			}

			if (InputHandler.KeyDown(Keys.Down))
				player2Motion.Y = 1;
			else if (InputHandler.KeyDown(Keys.Up))
				player2Motion.Y = -1;

			if (player2Motion != Vector2.Zero) {
				player2Motion.Normalize();
				Player2.Move(player2Motion * speed);
			}
		}

		void CheckCollisions(GameTime gameTime)
		{
			if (ball.Position.Y <= 0 || ball.Position.Y >= Root.ScreenHeight - ball.Bounds.Height) {
				ball.BounceWall();
				bounce.Play();
			}

			if (Player1.Bounds.Intersects(ball.Bounds)) {
				ball.BounceLeft(Player1.Position, Player1.Bounds.Height);
				bounce.Play();
			}

			if (Player2.Bounds.Intersects(ball.Bounds)) {
				ball.BounceRight(Player2.Position, Player2.Bounds.Height);
				bounce.Play();
			}

			if (ball.Position.X <= 0) {
				ball.Reset();
				scorePlayer2 += 1;
				goal.Play();
			}

			if (ball.Position.X >= Root.ScreenWidth - 30) {
				ball.Reset();
				scorePlayer1 += 1;
				goal.Play();
			}
		}
	}
}

