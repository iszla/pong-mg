﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using MonoGame.Utils;

namespace PongMG
{
	public class MenuScreen : GameState
	{
		string heading;
		string menuString;
		Vector2 textSize;

		public MenuScreen(Game game, GameStateManager gsm) : base(game, gsm)
		{
			heading = "PONG";
			menuString = "Press ENTER to play";
		}

		protected override void LoadContent()
		{
			base.LoadContent();
		}

		public override void Update(GameTime gameTime)
		{
			if (InputHandler.KeyReleased(Keys.Enter)) {
				InputHandler.Flush();
				gameStateManager.ChangeStates(Root.Play);
			}

			base.Update(gameTime);
		}

		public override void Draw(GameTime gameTime)
		{
			Root.Batch.Begin();

			textSize = Font50.MeasureString(heading);

			Root.Batch.DrawString(
				Font50,
				heading,
				new Vector2((Root.ScreenWidth / 2) - (textSize.X / 2), 250),
				Color.White);

			textSize = Font36.MeasureString(menuString);

			Root.Batch.DrawString(
				Font36,
				menuString,
				new Vector2((Root.ScreenWidth / 2) - (textSize.X / 2), 600),
				Color.White);

			base.Draw(gameTime);

			Root.Batch.End();
		}
	}
}

