﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using MonoGame.Utils;

namespace PongMG
{
	public abstract class GameState : BaseGameState
	{
		protected Root Root;
		protected SpriteFont Font50;
		protected SpriteFont Font36;
		protected SpriteFont DebugFont;
		protected ContentManager Content;

		public GameState(Game game, GameStateManager gsm) : base(game, gsm)
		{
			Root = (Root)game;
			Content = Root.Content;
		}

		protected override void LoadContent()
		{
			Font50 = Content.Load<SpriteFont>("Font/Futura50");
			Font36 = Content.Load<SpriteFont>("Font/Futura36");
			DebugFont = Content.Load<SpriteFont>("Font/Debug");

			base.LoadContent();
		}

		public override void Update(GameTime gameTime)
		{
			if (!Debug.Initialized) {
				Debug.Init(Root.Batch, DebugFont, 5);
				Debug.fpsCounter = true;
			}

			base.Update(gameTime);
		}

		public override void Draw(GameTime gameTime)
		{
			base.Draw(gameTime);
		}
	}
}

