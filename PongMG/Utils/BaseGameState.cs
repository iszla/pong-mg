﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace MonoGame.Utils
{
	public abstract partial class BaseGameState : DrawableGameComponent
	{
		List<GameComponent> childComponents;

		public List<GameComponent> Components {
			get { return childComponents; }
		}

		BaseGameState tag;

		public BaseGameState Tag {
			get { return tag; }
		}

		protected GameStateManager gameStateManager;

		DrawableGameComponent drawComponent;

		public BaseGameState(Game game, GameStateManager manager)
			: base(game)
		{
			gameStateManager = manager;
			childComponents = new List<GameComponent>();
			tag = this;
		}

		public override void Initialize()
		{
			base.Initialize();
		}

		public override void Update(GameTime gameTime)
		{

			foreach (var component in childComponents) {
				if (component.Enabled)
					component.Update(gameTime);
			}

			base.Update(gameTime);

#if DEBUG
			Debug.Update(gameTime);
#endif
		}

		public override void Draw(GameTime gameTime)
		{

			foreach (var component in childComponents) {
				if (component is DrawableGameComponent) {
					drawComponent = component as DrawableGameComponent;

					if (drawComponent.Visible)
						drawComponent.Draw(gameTime);
				}
			}

			base.Draw(gameTime);

#if DEBUG
			Debug.Draw(gameTime);
#endif
		}

		internal protected virtual void StateChange(object sender, EventArgs e)
		{
			if (gameStateManager.CurrentState == Tag)
				Show();
			else
				Hide();
		}

		protected virtual void Show()
		{
			Visible = true;
			Enabled = true;

			foreach (var component in childComponents) {
				component.Enabled = true;
				if (component is DrawableGameComponent)
					((DrawableGameComponent)component).Visible = true;
			}
		}

		protected virtual void Hide()
		{
			Visible = false;
			Enabled = false;

			foreach (var component in childComponents) {
				component.Enabled = false;
				if (component is DrawableGameComponent)
					((DrawableGameComponent)component).Visible = false;
			}
		}
	}
}

