﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGame.Utils
{
	public static class Debug
	{
		static List<DebugInfo> debugList = new List<DebugInfo>();
		static Dictionary<string, string> debugDict = new Dictionary<string, string>();
		static int displayTime, ySpacing;
		static SpriteBatch batch;
		static SpriteFont font;
		public static bool Initialized;
		static bool displayedError;

		// FPS Counter
		public static bool fpsCounter;
		static long TotalFrames;
		static float TotalSeconds;
		static float AverageFramesPerSecond;
		static float CurrentFramesPerSecond;

		public const int MAXIMUM_SAMPLES = 100;

		static Queue<float> _sampleBuffer = new Queue<float>();

		public static void Init(SpriteBatch spriteBatch, SpriteFont spriteFont, int display)
		{
			debugList = new List<DebugInfo>();
			debugDict = new Dictionary<string, string>();
			batch = spriteBatch;
			font = spriteFont;
			displayTime = display;
			Vector2 spacing = font.MeasureString("TEXT");
			ySpacing = (int)spacing.Y + 3;
			Initialized = true;
		}

		public static void Update(GameTime gameTime)
		{
			if (debugList.Count == 0)
				return;

			for (int i = 0; i < debugList.Count; i++) {
				if ((debugList[i].Time + displayTime) < gameTime.TotalGameTime.Seconds) {
					debugList.RemoveAt(i);
				}
			}
		}

		public static void Draw(GameTime gameTime)
		{
			if (!Initialized) {
				if (!displayedError) {
					LogError("Error: Debug not initialized. Use Debug.Init()");
					displayedError = true;
				}
				return;
			}
			int ypos = 0;

			if (fpsCounter) {
				CurrentFramesPerSecond = 1.0f / (float)gameTime.ElapsedGameTime.TotalSeconds;

				_sampleBuffer.Enqueue(CurrentFramesPerSecond);

				if (_sampleBuffer.Count > MAXIMUM_SAMPLES) {
					_sampleBuffer.Dequeue();
					AverageFramesPerSecond = _sampleBuffer.Average(i => i);
				} else {
					AverageFramesPerSecond = CurrentFramesPerSecond;
				}

				TotalFrames++;
				TotalSeconds += (float)gameTime.ElapsedGameTime.TotalSeconds;
				ypos += ySpacing;

				batch.DrawString(font, "FPS: " + AverageFramesPerSecond, new Vector2(0, 0), Color.White);
			}

			if (debugDict.Count > 0) {
				foreach (string key in debugDict.Keys) {
					batch.DrawString(font, debugDict[key], new Vector2(0, ypos), Color.White);
					ypos += ySpacing;
				}
			}

			if (debugList.Count > 0) {
				for (int i = 0; i < debugList.Count; i++) {
					batch.DrawString(font, debugList[i].Message, new Vector2(0, ypos), Color.White);
					ypos += ySpacing;
				}
			}
		}

		public static void Log(object s)
		{
			Console.WriteLine(s.ToString());
		}

		public static void LogWarning(object s)
		{
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine(s.ToString());
			Console.ResetColor();
		}

		public static void LogError(object s)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(s.ToString());
			Console.ResetColor();
		}

		public static void LogToScreen(object s, GameTime gameTime)
		{
			debugList.Add(new DebugInfo(s.ToString(), gameTime.TotalGameTime.Seconds));
		}

		public static void PutOnScreen(string s, string data)
		{
			if (debugDict.ContainsKey(s)) {
				debugDict[s] = data;
				return;
			}

			debugDict.Add(s, data);
		}


	}
}

