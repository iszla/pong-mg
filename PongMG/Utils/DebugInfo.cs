﻿namespace MonoGame.Utils
{
	public class DebugInfo
	{
		string message;
		int time;

		public string Message {
			get { return message; }
		}

		public int Time {
			get { return time; }
		}

		public DebugInfo(string m, int t)
		{
			message = m;
			time = t;
		}
	}
}

