﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoGame.Utils
{
	public enum MouseButtons { Left, Right, Middle, ScrollUp, ScrollDown }

	public class InputHandler : GameComponent
	{
		static KeyboardState keyboardState;
		static KeyboardState lastKeyboardState;

		static GamePadState[] gamePadStates;
		static GamePadState[] lastGamePadStates;

		static MouseState mouseState;
		static MouseState lastMouseState;

		/// <summary>
		/// Get the current KeyboardState
		/// </summary>
		/// <value>Current KeyboardState</value>
		public static KeyboardState KeyboardState {
			get { return keyboardState; }
		}

		/// <summary>
		/// Get the previous KeyboardState
		/// </summary>
		/// <value>Previous KeyboardState</value>
		public static KeyboardState LastKeyboardState {
			get { return lastKeyboardState; }
		}

		/// <summary>
		/// Gets the current GamePadStates.
		/// </summary>
		/// <value>Current GamePadStates</value>
		public static GamePadState[] GamePadStates {
			get { return gamePadStates; }
		}

		/// <summary>
		/// Gets the previous GamePadStates
		/// </summary>
		/// <value>Previous GamePadStates</value>
		public static GamePadState[] LastGamePadStates {
			get { return lastGamePadStates; }
		}

		/// <summary>
		/// Gets the current MouseState
		/// </summary>
		/// <value>Current MouseState</value>
		public static MouseState MouseState {
			get { return mouseState; }
		}

		/// <summary>
		/// Gets the previous MouseState
		/// </summary>
		/// <value>Previous MouseState</value>
		public static MouseState LastMouseState {
			get { return lastMouseState; }
		}

		public InputHandler(Game game)
			: base(game)
		{
			keyboardState = Keyboard.GetState();

			mouseState = Mouse.GetState();

			gamePadStates = new GamePadState[Enum.GetValues(typeof(PlayerIndex)).Length];

			foreach (PlayerIndex index in Enum.GetValues(typeof(PlayerIndex))) {
				gamePadStates[(int)index] = GamePad.GetState(index);
			}
		}

		public override void Initialize()
		{
			base.Initialize();
		}

		public override void Update(GameTime gameTime)
		{
			lastKeyboardState = keyboardState;
			keyboardState = Keyboard.GetState();

			lastMouseState = mouseState;
			mouseState = Mouse.GetState();

			lastGamePadStates = (GamePadState[])gamePadStates.Clone();
			foreach (PlayerIndex index in Enum.GetValues(typeof(PlayerIndex))) {
				gamePadStates[(int)index] = GamePad.GetState(index);
			}

			base.Update(gameTime);
		}

		/// <summary>
		/// Reset the KeyboardState
		/// </summary>
		public static void Flush()
		{
			lastKeyboardState = keyboardState;
			lastMouseState = mouseState;
			lastGamePadStates = (GamePadState[])gamePadStates.Clone();
		}

		/// <summary>
		/// Check if the key has just been released
		/// </summary>
		/// <returns><c>true</c>, if key was released, <c>false</c> otherwise.</returns>
		/// <param name="key">Key</param>
		public static bool KeyReleased(Keys key)
		{
			return keyboardState.IsKeyUp(key) && lastKeyboardState.IsKeyDown(key);
		}

		/// <summary>
		/// Check if the key has just been pressed
		/// </summary>
		/// <returns><c>true</c>, if key was pressed, <c>false</c> otherwise.</returns>
		/// <param name="key">Key</param>
		public static bool KeyPressed(Keys key)
		{
			return keyboardState.IsKeyDown(key) && lastKeyboardState.IsKeyUp(key);
		}

		/// <summary>
		/// Check if the key is down
		/// </summary>
		/// <returns><c>true</c>, if key is down, <c>false</c> otherwise.</returns>
		/// <param name="key">Key</param>
		public static bool KeyDown(Keys key)
		{
			return keyboardState.IsKeyDown(key);
		}

		public static bool ButtonReleased(Buttons button, PlayerIndex index)
		{
			return gamePadStates[(int)index].IsButtonUp(button) &&
			lastGamePadStates[(int)index].IsButtonDown(button);
		}

		public static bool ButtonPressed(Buttons button, PlayerIndex index)
		{
			return gamePadStates[(int)index].IsButtonDown(button) &&
			lastGamePadStates[(int)index].IsButtonUp(button);
		}

		public static bool ButtonDown(Buttons button, PlayerIndex index)
		{
			return gamePadStates[(int)index].IsButtonDown(button);
		}

		public static bool MouseReleased(MouseButtons button)
		{
			switch (button) {
				case MouseButtons.Left:
					return mouseState.LeftButton == ButtonState.Released &&
						   lastMouseState.LeftButton == ButtonState.Pressed;
				case MouseButtons.Right:
					return mouseState.RightButton == ButtonState.Released &&
						   lastMouseState.RightButton == ButtonState.Pressed;
				case MouseButtons.Middle:
					return mouseState.MiddleButton == ButtonState.Released &&
						   lastMouseState.MiddleButton == ButtonState.Pressed;
				case MouseButtons.ScrollUp:
					return mouseState.ScrollWheelValue > lastMouseState.ScrollWheelValue;
				case MouseButtons.ScrollDown:
					return mouseState.ScrollWheelValue < lastMouseState.ScrollWheelValue;
				default:
					return false;
			}
		}

		public static bool MousePressed(MouseButtons button)
		{
			switch (button) {
				case MouseButtons.Left:
					return mouseState.LeftButton == ButtonState.Pressed &&
					lastMouseState.LeftButton == ButtonState.Released;
				case MouseButtons.Right:
					return mouseState.RightButton == ButtonState.Pressed &&
					lastMouseState.RightButton == ButtonState.Released;
				case MouseButtons.Middle:
					return mouseState.MiddleButton == ButtonState.Pressed &&
					lastMouseState.MiddleButton == ButtonState.Released;
				case MouseButtons.ScrollUp:
					return mouseState.ScrollWheelValue > lastMouseState.ScrollWheelValue;
				case MouseButtons.ScrollDown:
					return mouseState.ScrollWheelValue < lastMouseState.ScrollWheelValue;
				default:
					return false;
			}
		}

		public static bool MouseDown(MouseButtons button)
		{
			switch (button) {
				case MouseButtons.Left:
					return mouseState.LeftButton == ButtonState.Pressed;
				case MouseButtons.Right:
					return mouseState.RightButton == ButtonState.Pressed;
				case MouseButtons.Middle:
					return mouseState.MiddleButton == ButtonState.Pressed;
				case MouseButtons.ScrollUp:
					return mouseState.ScrollWheelValue > lastMouseState.ScrollWheelValue;
				case MouseButtons.ScrollDown:
					return mouseState.ScrollWheelValue < lastMouseState.ScrollWheelValue;
				default:
					return false;
			}
		}

		public static bool MouseEdge(string edge, Rectangle bounds)
		{
			// If Mouse Position is negative, return false
			// Fixes bug where KB controls did not work unless the mouse had been
			// moved at all since the start of the game
			if (mouseState.Position.X < 0 || mouseState.Position.Y < 0)
				return false;

			switch (edge) {
				case "left":
				case "LEFT":
					return mouseState.Position.X < bounds.Left + 10;
				case "right":
				case "RIGHT":
					return mouseState.Position.X > bounds.Right - 10;
				case "top":
				case "TOP":
					return mouseState.Position.Y < bounds.Top + 10;
				case "bottom":
				case "BOTTOM":
					return mouseState.Position.Y > bounds.Bottom - 10;
				default:
					return false;
			}
		}
	}
}

