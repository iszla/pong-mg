﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using MonoGame.Utils;

namespace PongMG
{
	public class Ball
	{
		Texture2D texture;
		Vector2 position;
		Vector2 motion;
		Rectangle bounds;
		float speed;
		Random rand;

		public Rectangle Bounds {
			get { return bounds; }
		}

		public float Speed {
			get { return speed; }
			set { speed = value; }
		}

		public Vector2 Position {
			get { return position; }
		}

		public Ball(Texture2D tex)
		{
			rand = new Random();
			texture = tex;
			Reset();
		}

		public void Draw(SpriteBatch batch)
		{
			batch.Draw(texture, position, Color.White);
		}

		public void Update(GameTime gameTime)
		{
			position += motion * speed;

			UpdateBounds();
		}

		public void BounceLeft(Vector2 batPos, int batHeight)
		{
			float y = (position.Y - batPos.Y) / batHeight;
			y -= 0.25f;
			motion = Vector2.Normalize(new Vector2(1, y));

			speed += 0.1f;
		}

		public void BounceRight(Vector2 batPos, int batHeight)
		{
			float y = (position.Y - batPos.Y) / batHeight;
			y -= 0.25f;
			motion = Vector2.Normalize(new Vector2(-1, y));

			speed += 0.1f;
		}

		public void BounceWall()
		{
			motion.Y *= -1;
		}

		void UpdateBounds()
		{
			bounds.X = (int)position.X;
			bounds.Y = (int)position.Y;
		}

		public void Reset()
		{
			position = new Vector2(Root.ScreenWidth / 2 - 15, Root.ScreenHeight / 2 - 15);
			speed = 0;

			float x, y;
			do {
				x = 150 * (float)Math.Cos(rand.Next(0, 360));
			} while (x > -50 && x < 50);

			do {
				y = 150 * (float)Math.Sin(rand.Next(0, 360));
			} while (y > -50 && y < 50);


			motion = Vector2.Normalize(new Vector2(x, y));
			bounds = new Rectangle(
				(int)position.X,
				(int)position.Y,
				texture.Width,
				texture.Height);

			Debug.Log("(" + x + ", " + y + ")");
		}
	}
}

