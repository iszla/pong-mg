﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PongMG
{
	public class Bat
	{
		Texture2D texture;
		Vector2 position;
		Rectangle bounds;

		public Rectangle Bounds {
			get { return bounds; }
		}

		public Vector2 Position {
			get { return position; }
		}

		public Bat(Texture2D tex, Vector2 pos)
		{
			texture = tex;
			position = pos;
			bounds = new Rectangle(
					(int)pos.X,
					(int)pos.Y,
					texture.Width,
					texture.Height);
		}

		public void Draw(SpriteBatch batch)
		{
			batch.Draw(texture, position, Color.White);
		}

		public void Move(Vector2 motion)
		{
			position += motion;
			if (position.Y < 0)
				position.Y = 0;

			if (position.Y > Root.ScreenHeight - texture.Height)
				position.Y = Root.ScreenHeight - texture.Height;

			bounds.X = (int)position.X;
			bounds.Y = (int)position.Y;
		}
	}
}

